from .routers.router import Router
from .codecs.codec import Codec
from .find import Find

import aspyre.errors as errors
import aspyre.dictionary as dictionary


class AspyreGroup:
    def __init__(self, router, codec):
        if not isinstance(router, Router):
            raise TypeError('router must descend from aspyre.routers.router.Router')

        if not isinstance(codec, Codec):
            raise TypeError('codec must descend from aspyre.codecs.codec.Codec')

        self.__router = router
        self.__codec = codec

    # Given a domain, path, and HTTP method, find all the handlers that can handle it.
    async def find(self, host, path, method):
        router_handlers = self.__router.find(host, path)

        if router_handlers:
            before = 'before_' + method
            after = 'after_' + method

            handlers = OrderedDict()
            handlers['before'] = []
            handlers[before] = []
            handlers[method] = []
            handlers[after] = []
            handlers['after'] = []

            for handler in router_handlers:
                args = handler[1]
                kwargs = handler[2]
                handler = handler[0]

                if callable(getattr(handler, 'before', None)):
                    handlers['before'].append((handler.before, args, kwargs))
                if callable(getattr(handler, before, None)):
                    handlers[before].append((getattr(handler, before), args, kwargs))
                if callable(getattr(handler, method, None)):
                    handlers[method].append((getattr(handler, method), args, kwargs))
                if callable(getattr(handler, after, None)):
                    handlers[after].append((getattr(handler, after), args, kwargs))
                if callable(getattr(handler, 'after', None)):
                    handlers[after].append(handler.after, args, kwargs)

            return handlers

        return None

    async def handle(self, handlers, body, headers, query_string):
        pass


class AspyreApplication:
    def __init__(self, find=Find.first):
        self.__groups = []
        self.__find = find

    async def __call__(self, host, path, method, headers, body, query_string, context):
        # ToDo: Find the best handlers (using __find) and call them sequentially.
        return 200, [('Test', 'True')], '{"hello": "world"}'

    # Add a new handler group to the Application.
    def add_group(self, group):
        if not isinstance(group, AspyreGroup):
            raise TypeError("AspyreApplication.add_group only accepts instances of 'AspyreGroup' only. You gave a " \
                    "type of '{}'.".format(type(group).__name__))

        if group in self.__groups:
            raise ValueError('AspyreGroup has already been added to the AspyreApplication.')

        self.__groups.append(group)

    def add_groups(self, *groups):
        for group in groups:
            self.add_group(group)
