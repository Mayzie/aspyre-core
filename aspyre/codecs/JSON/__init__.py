from aspyre.codecs.codec import Codec

import aspyre.dictionary as dictionary

class JSONCodec(Codec):
    def __init__(self, strict=False):
        self.__strict = strict

    def encode(self, output):
        headers = {
            'Content-Type': 'application/json',
        }

        if '__headers' in output:
            headers = {**output['__headers'], **headers}

        return bytes()

    def decode(self, body, headers):
        if not self.__strict and not getattr(headers, 'content-type', None) == 'application/json':
            raise Exception('Missing or invalid Content-Type supplied.')

        return dictionary.AspyreDictImmutable()

    def get_immutable_class(self):
        return dictionary.AspyreDictImmutable

    def get_mutable_class(self):
        return dictionary.AspyreDictHistory
