__all__ = [
    'Find'
]

class Find:
    # Return the very first group that returns at least one handler. 
    @staticmethod
    async def first(host, path, method, groups):
        for group in groups:
            handlers = await group.find(host, path, method)
            if handlers:
                return handlers, group

        return None

    # Returns the group that has the most handlers.
    async def best(host, path, method, groups):
        pass
