from types import ModuleType  # To check if `regex_library` (__init__) is a module.

from aspyre.routers.router import Router


__all__ = [
    'RegexRouter',
]


class RegexRouter(Router):
    __required_methods = ['compile', 'fullmatch']

    def __init__(self, regex_library=None):
        if regex_library is None:
            import re
            self.__re = re
        else:
            # Naively validate that the the regex library supports the callables in __required_methods.
            for method in RegexRouter.__required_methods:
                if not hasattr(regex_library, method):
                    raise TypeError("regex_library must have callable member '{}'".format(method))

            # It passed the test.
            self.__re = regex_library

        self._url_list = []

    def add_route(self, cls, *urls):
        for url in urls:
            host = None
            path = None

            # Check if URL is a tuple of two string elements.
            if isinstance(url, tuple) and len(url) == 2:
                if url[0] is not None and not isinstance(url[0], str):
                    raise ValueError("URLs that are tuples must contain strings or None. You gave '{}' " \
                        "of type '{}' as the first element".format(url[0], type(url[0]).__name__))
                if url[1] is not None and not isinstance(url[1], str):
                    raise ValueError("URLs that are tuples must contain strings or None. You have '{}' " \
                        "of type '{}' as the second element".format(url[1], type(url[1]).__name__))

                # Add the `host` portion.
                if url[0]:
                    host = self.__re.compile(url[0])
                # Add the `path` portion.
                if url[1]:
                    path = self.__re.compile(url[1])
            # Check if url is a string only.
            elif isinstance(url, str):
                path = self.__re.compile(url)
            else:
                raise ValueError("URLs must either be a string (for path matching only), or a tuple of two " \
                    "strings (for matching both the host, and path, respectively). You gave '{}' of type " \
                    "'{}'".format(url, type(url).__name__))

            # Handle any groupings within the host and path regular expressions.
            path_groups = path.groups

            path_named_groups = path.groupindex

            # ToDo: Check if separating them out is pointless.
            # Create an empty list of size of the number of non-named groups.
            host_args = None
            path_args = None

            # Create an empty dictionary with elements for each of the named groups.
            # host_kwargs = {k: None for k, v in host_named_groups.items()}
            # path_kwargs = {k: None for k, v in path_named_groups.items()}

            # Store the positions of the named groups (so we can ignore them later).
            path_kwargs_idx = [v - 1 for k, v in path_named_groups.items()]

            # ToDo: Cache number of groups 
            if host:
                host_groups = host.groups
                host_named_groups = host.groupindex
                host_kwargs_idx = [v - 1 for k, v in host_named_groups.items()]
            else:
                host_groups = None
                host_named_groups = None
                host_kwargs_idx = []

            # (host groupings, path groupings)
            groupings = (host_kwargs_idx, path_kwargs_idx)

            self._url_list.append(((host, path), cls, groupings))

    def find(self, host, path):
        handlers = []

        for u in self._url_list:
            url = u[0]  # Extract the (host, path) regex objects.
            cls = u[1]  # Extract the handling class.
            groupings = u[2]  # Extract any grouping information.

            if isinstance(url, tuple) and len(url) == 2:
                url_host = url[0]
                url_path = url[1]
            else:
                url_host = None
                url_path = url

            m1, m2 = None, None

            # args = []  # Store the results of any groupings within the regex.
            # kwargs = {}
            host_args = None
            path_args = None

            host_kwargs = {}
            path_kwargs = {}

            # First match (matching the `host`)
            if url_host:
                host_kwargs_idx = groupings[0]

                m1 = self.__re.fullmatch(url_host, host)
                if m1:
                    host_kwargs.update(m1.groupdict())
                    m1_groups = m1.groups()
                    host_args = [v for i, v in zip(range(len(m1_groups)), m1_groups) if i not in host_kwargs_idx]

            # Second match (matching the `path`)
            if url_path:
                path_kwargs_idx = groupings[1]

                m2 = self.__re.fullmatch(url_path, path)
                if m2:
                    path_kwargs.update(m2.groupdict())
                    m2_groups = m2.groups()
                    path_args = [v for i, v in zip(range(len(m2_groups)), m2_groups) if i not in path_kwargs_idx]

            if m1 or m2:
                args = []
                if host_args:
                    args += host_args
                if path_args:
                    args += path_args

                kwargs = {**host_kwargs, **path_kwargs}

                if cls not in handlers:
                    handlers.append((cls, args, kwargs))

        return handlers
