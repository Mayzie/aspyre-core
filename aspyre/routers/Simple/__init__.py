import re

from random import choice
from string import ascii_letters, digits

from aspyre.routers.Regex import RegexRouter

# Import all the necessary modules for __default_types (below)
import datetime
import uuid
import base64

__all__ = [
    'SimpleRouter',
]

__combined_chars = ascii_letters + digits
# Add some vanity.
_separator = '0_d_' + ''.join([choice(__combined_chars) for _ in range(8)]) + '_collis_1'

# Types and their functions that will be called to convert a URL arguments to that type.
_default_types = {
    'str': str,
    'strl': str.lower,
    'stru': str.upper,
    'int': int,
    'float': float,
    'date': lambda s: datetime.date(*map(int, s.split('-'))),
    'timestamp': lambda s: datetime.datetime.fromtimestamp(int(s)),
    'uuid': uuid.UUID,
    'base64': lambda s: str(base64.urlsafe_b64decode(s), 'ascii'),
}

_re_args = re.compile('\<[^\>]*\>')


class SimpleRouter(RegexRouter):
    def __init__(self, types=None):
        super().__init__(regex_library=re)

        if not types:
            self.__types = _default_types
        else:
            self.__types = types

    def add_route(self, cls, *urls):
        for url in urls:
            if not isinstance(url, str):
                raise TypeError("URLs passed to add_route must be a string type for Simple routing. You " \
                        "gave '{}' of type '{}'.".format(url, type(url).__name__))

            # Remove any leading or trailing slashes.
            url = url.lstrip('/').rstrip('/')

            last_index = 0
            parsed_url = '/'
            for m in _re_args.finditer(url):
                # Copy the existing URL up until the match point.
                parsed_url += re.escape(url[last_index:m.start()])
                # Strip the angle brackets and store the type and name.
                _type, name = m.group()[1:][:-1].split(':')
                # Verify that the type is a recognised type.
                if _type not in self.__types and _type not in _default_types:
                    raise ValueError("'{}' is not a valid type in '<{}:{}>' in URL '/{}'.".format(type, type, name, url))
                # Create a named group out of the type and name.
                group = SimpleRouter.group(_type, name)
                # Append the new Regex named group.
                parsed_url += group
                # Recalculate and update last_index
                last_index = m.start() + len(m.group())

            if last_index != len(url):
                parsed_url += re.escape(url[last_index:])

            super().add_route(cls, parsed_url)

    def find(self, host, path):
        handlers = super().find(host, path)
        ret_handlers = []

        for i in range(len(handlers)):
            h = handlers[i]

            new_kwargs = {}
            kwargs = h[2]
            exit = False
            for key, value in kwargs.items():
                exit = True
                # Extract the type and name from the named group name.
                type, name = key.split(_separator)

                # Execute the type-cast.
                # Note: If the cast fails, we do not want an exception to be raised, as the URL clearly
                # did not "match".
                if type in self.__types:
                    try:
                        value = self.__types[type](value)
                    except:
                        continue
                elif type in _default_types:
                    try:
                        value = _default_types[type](value)
                    except:
                        continue
                else:
                    continue

                exit = False

                new_kwargs[name] = value

            if not exit:
                ret_handlers.append((h[0], [], new_kwargs))

        return ret_handlers

    @staticmethod
    def group(type, name):
        return '(?P<{}{}{}>[^/]+)'.format(type, _separator, name)
