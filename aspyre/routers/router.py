from abc import ABCMeta, abstractmethod

__all__ = ['Router']


class Router(metaclass=ABCMeta):
    @abstractmethod
    def __init__(self, types):
        pass

    @abstractmethod
    def add_route(self, cls, *args):
        pass

    @abstractmethod
    def find(self, host, path):
        pass

    @abstractmethod
    def get_classes(self):
        pass
