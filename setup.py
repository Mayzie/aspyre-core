from setuptools import setup, find_packages

setup(
    name='aspyre',
    version='0.1.0',
    description='The core Aspyre library, necessary to run Aspyre applications.',
    url='aspy.re',
    author='Mayzie',
    license='MIT',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],
    packages=['aspyre', 'aspyre.plugins', 'aspyre.routers', 'aspyre.codecs', 'aspyre.servers'],
    #install_requires=['aspyre'],
    python_requires='~=3.5'
)
