#!/usr/bin/env python

from aspyre.routers.Regex import RegexRouter

class C:
    def post(*args, **kwargs):
        print('Args: ' + str(args))
        print('KWArgs: ' + str(kwargs))


r = RegexRouter()
r.add_route(C, ('.*\.example.com', '/hello/([^/]+)/world'))
r.add_route(C, ('.*\.example.com', '/hello/(?P<first_name>\w+) (?P<last_name>\w+)/world'))
r.add_route(C, ('.*\.example.com', '/([^/]+)/(?P<first_name>\w+) (?P<last_name>\w+)/([^/]+)'))

m = r.find('www.example.com', '/hello/Malcolm Reynolds/world')

for c in m:
    c[0].post(*(c[1]), **(c[2]))
