#!/usr/bin/env python

from aspyre.routers.Simple import SimpleRouter

def test_no_type():
    class C:
        def get():
            pass

    r = SimpleRouter()
    r.add_route(C, '/')
    
    handlers = r.find(None, '/')
    assert len(handlers) == 1

    h = handlers[0]
    assert h[0] == C
    assert not h[1]
    assert not h[2]

    h[0].get()

def test_no_type_multiple():
    class C:
        def get():
            pass

    r = SimpleRouter()
    r.add_route(C, '/')
    r.add_route(C, '/hello/world')

    handlers = r.find(None, '/')
    assert len(handlers) == 1

    h = handlers[0]
    assert h[0] == C
    assert not h[1]
    assert not h[2]

    h[0].get()

    handlers = r.find(None, '/hello/world')
    assert len(handlers) == 1

    h = handlers[0]
    assert h[0] == C
    assert not h[1]
    assert not h[2]

    h[0].get()

def test_type():
    class C:
        def get(name):
            assert name == 'Daniel Collis'

    r = SimpleRouter()
    r.add_route(C, '/<str:name>')

    handlers = r.find(None, '/Daniel Collis')
    assert len(handlers) == 1

    h = handlers[0]
    assert h[0] == C
    assert not h[1]
    assert len(h[2]) == 1

    h[0].get(**h[2])

def test_type_multiple():
    class C:
        def get(name, gender):
            assert name == 'Daniel Collis'
            assert gender == 'M'

    r = SimpleRouter()
    r.add_route(C, '/<str:name>/<str:gender>')

    handlers = r.find(None, '/Daniel Collis/M')
    assert len(handlers) == 1

    h = handlers[0]
    assert h[0] == C
    assert not h[1]
    assert len(h[2]) == 2

    h[0].get(**h[2])

def test_type_separate():
    class C:
        def name(name):
            assert name == 'Daniel Collis'
        def age(age):
            assert age == 23

    r = SimpleRouter()
    r.add_route(C, '/<str:name>')
    r.add_route(C, '/<int:age>/age')

    handlers = r.find(None, '/Daniel Collis')
    assert len(handlers) == 1

    h = handlers[0]
    assert h[0] == C
    assert not h[1]
    assert len(h[2]) == 1

    h[0].name(**h[2])

    handlers = r.find(None, '/23/age')
    assert len(handlers) == 1

    h = handlers[0]
    assert h[0] == C
    assert not h[1]
    assert len(h[2]) == 1

    h[0].age(**h[2])

test_no_type()
test_no_type_multiple()

test_type()
test_type_multiple()
test_type_separate()
